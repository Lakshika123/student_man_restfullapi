package com.sgic.employee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgic.employee.entities.Student;
import com.sgic.employee.repositories.StudentRepository;

import java.util.List;

@Service 
public class StudentServiceImpl implements StudentService {
@Autowired 
StudentRepository studentRepository;

	@Override
	public void saveStudent(Student student) {
		studentRepository.save(student);
	}

	@Override
	public List allStudent(){
		return studentRepository.findAll();
	}

	@Override
	public Student getStudentById(Long id){
		Student student = studentRepository.findById(id).orElseThrow(null);
		return student;
	}

	@Override
	public Student updateStudent(Long id, Student student){
		Student empl = studentRepository.findById(id).orElseThrow(null);
		empl.setEmail(student.getEmail());
		empl.setFirstName(student.getFirstName());
		empl.setLastName(student.getLastName());
		studentRepository.save(empl);
		return empl;

	}

	@Override
	public String deleteStudent(Long id){
		Student student = studentRepository.findById(id).orElseThrow(null);
		studentRepository.delete(student);

		return "SUCCESSFULLY DELETED";
	}


}
