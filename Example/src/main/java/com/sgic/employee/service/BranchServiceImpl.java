package com.sgic.employee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgic.employee.entities.Branch;
import com.sgic.employee.repositories.BranchRepository;

import java.util.List;

@Service
public class BranchServiceImpl implements BranchService {
@Autowired
BranchRepository branchRepository;

	@Override
	public void saveBranch(Branch branch) {
		branchRepository.save(branch);
	}

	@Override
	public List allBranch(){
		return branchRepository.findAll();
	}

	@Override
	public Branch getBranchById(Long id){
		Branch branch = branchRepository.findById(id).orElseThrow(null);
		return branch;
	}

	/*@Override
	public Student updateStudent(Long id, Student student){
		Student empl = studentRepository.findById(id).orElseThrow(null);
		empl.setEmail(student.getEmail());
		empl.setFirstName(student.getFirstName());
		empl.setLastName(student.getLastName());
		studentRepository.save(empl);
		return empl;

	}

	@Override
	public String deleteStudent(Long id){
		Student student = studentRepository.findById(id).orElseThrow(null);
		studentRepository.delete(student);

		return "SUCCESSFULLY DELETED";
	}*/


}
