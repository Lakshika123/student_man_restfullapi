package com.sgic.employee.service;

import com.sgic.employee.entities.Course;

import java.util.List;

public interface CourseService {
	 void saveCourse(Course course);
	 List allCourse();
	 Course getCourseById(Long id);
	 Course updateCourse(Long id, Course course);
	 String deleteCourse(Long id);

}
