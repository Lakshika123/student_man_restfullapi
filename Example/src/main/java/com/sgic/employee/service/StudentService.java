package com.sgic.employee.service;

import com.sgic.employee.entities.Student;

import java.util.List;

public interface StudentService {
	 void saveStudent(Student student);
	 List allStudent();
	 Student getStudentById(Long id);
	 Student updateStudent(Long id, Student student);
	 String deleteStudent(Long id);

}
