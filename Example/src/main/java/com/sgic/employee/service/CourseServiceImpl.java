package com.sgic.employee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgic.employee.entities.Course;
import com.sgic.employee.repositories.CourseRepository;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
@Autowired
CourseRepository courseRepository;

	@Override
	public void saveCourse(Course course) {
		courseRepository.save(course);
	}

	@Override
	public List allCourse(){
		return courseRepository.findAll();
	}

	@Override
	public Course getCourseById(Long id){
		Course course = courseRepository.findById(id).orElseThrow(null);
		return course;
	}

	@Override
	public Course updateCourse(Long id, Course course){
		Course empl = courseRepository.findById(id).orElseThrow(null);
		empl.setName(course.getName());
		empl.setDuration(course.getDuration());
		empl.setFee(course.getFee());
		courseRepository.save(empl);
		return empl;

	}

	@Override
	public String deleteCourse(Long id){
		Course course = courseRepository.findById(id).orElseThrow(null);
		courseRepository.delete(course);

		return "SUCCESSFULLY DELETED";
	}


}
