package com.sgic.employee.service;

import com.sgic.employee.entities.Branch;

import java.util.List;

public interface BranchService {
	 void saveBranch(Branch branch);
	 List allBranch();
	 Branch getBranchById(Long id);
	 //Student updateStudent(Long id, Student student);
	 //String deleteStudent(Long id);

}
