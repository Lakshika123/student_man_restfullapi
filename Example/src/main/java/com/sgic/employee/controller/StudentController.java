package com.sgic.employee.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sgic.employee.entities.Student;
import com.sgic.employee.service.StudentService;


/*@Controller ,@ResponseBody annotaion uses HTTP msg converters 
 to convert the return value to HTTP response body
 */
@RestController
public class StudentController {
	@Autowired
	StudentService studentService; //inject the StudentService dependencies...

	//Handle HTTP post method
	//http://localhost:8080/addStudent
	
	@PostMapping("/addStudent")  //Serve as rest end point 
	public ResponseEntity<Object> createIncomingSample(@RequestBody Student student) {
		studentService.saveStudent(student);
		return ResponseEntity.ok("Saved Successfully!");
	}//@Request body annotation use to convert request body into java object

	//this method annotate with @GetMapping annotation
	//this rest api configure 
	@GetMapping("/getAllStudent")
	public List<Student> allStudent(){
		return studentService.allStudent();
	}

	//@pathvariable annotation we bind the request URL template path variable 
	//to the method variable
	@GetMapping("/getStudent/{id}")
	public ResponseEntity<Student> getStudentById(@PathVariable Long id){
		Student student = studentService.getStudentById(id);
		return ResponseEntity.ok(student);
	}
	
	

	@PutMapping("/updateStudent/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student){
		Student empl = studentService.updateStudent(id, student);
		return ResponseEntity.ok(empl);
	}

	@DeleteMapping("/deleteStudent/{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable Long id){
		return ResponseEntity.ok( studentService.deleteStudent(id) );
	}

}
