package com.sgic.employee.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sgic.employee.entities.Course;
import com.sgic.employee.service.CourseService;

@RestController
public class CourseController {
	@Autowired
	CourseService courseService;

	@PostMapping("/addCourse")
	public ResponseEntity<Object> createIncomingSample(@RequestBody Course course) {
		courseService.saveCourse(course);
		return ResponseEntity.ok("Saved Successfully!");
	}

	@GetMapping("/getAllCourse")
	public List<Course> allCourse(){
		return courseService.allCourse();
	}

	@GetMapping("/getCourse/{id}")
	public ResponseEntity<Course> getCourseById(@PathVariable Long id){
		Course course = courseService.getCourseById(id);
		return ResponseEntity.ok(course);
	}

	@PutMapping("/updateCourse/{id}")
	public ResponseEntity<Course> updateCourse(@PathVariable Long id, @RequestBody Course course){
		Course empl = courseService.updateCourse(id, course);
		return ResponseEntity.ok(empl);
	}

	@DeleteMapping("/deleteCourse/{id}")
	public ResponseEntity<String> deleteCourse(@PathVariable Long id){
		return ResponseEntity.ok( courseService.deleteCourse(id) );
	}

}


