package com.sgic.employee.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sgic.employee.entities.Branch;
import com.sgic.employee.service.BranchService;

@RestController
public class BranchController {
	@Autowired
	BranchService branchService;

	@PostMapping("/addBranch")
	public ResponseEntity<Object> createIncomingSample(@RequestBody Branch branch) {
		branchService.saveBranch(branch);
		return ResponseEntity.ok("Saved Successfully!");
	}

	@GetMapping("/getAllBranch")
	public List<Branch> allBranch(){
		return branchService.allBranch();
	}

	@GetMapping("/getBranch/{id}")
	public ResponseEntity<Branch> getBranchById(@PathVariable Long id){
		Branch branch = branchService.getBranchById(id);
		return ResponseEntity.ok(branch);
	}

	/*@PutMapping("/updateStudent/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student){
		Student empl = studentService.updateStudent(id, student);
		return ResponseEntity.ok(empl);
	}

	@DeleteMapping("/deleteStudent/{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable Long id){
		return ResponseEntity.ok( studentService.deleteStudent(id) );
	}*/

}
