package com.sgic.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*SpringBootApplication annotation internally used 
@SpringBootConfiguration ---- internally annotated with @Configuration
                         ---- Used to convert java class into spring configuration class
@EnableAutoConfiguration ---- internally annotated with @AutoConfigurationpackage
						 
@ComponentScan			-----classes in the packages scan
*/

@SpringBootApplication
public class ExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleApplication.class, args);
	}

}
//Our springboot application will run embeded TomCat server 
//Spring boot application can be run as stand along