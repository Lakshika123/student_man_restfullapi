package com.sgic.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgic.employee.entities.Student;

public interface StudentRepository extends JpaRepository<Student, Long>{

}
//JpaRepository require two parameters, 
//-----------1. jpaEntity   2.Data type of the primary key
//No need to add @Repostitory annotation to StudentRepository interface
//Spring Data JPA internally provides @Repository Annotation

//No need add @Transactional Annotation to service class
//Spring web jpa provide it...