package com.sgic.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgic.employee.entities.Course;

public interface CourseRepository extends JpaRepository<Course, Long>{

}
