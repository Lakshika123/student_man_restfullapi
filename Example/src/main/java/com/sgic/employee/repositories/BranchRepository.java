package com.sgic.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgic.employee.entities.Branch;

public interface BranchRepository extends JpaRepository<Branch, Long>{

}
